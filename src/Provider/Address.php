<?php

namespace Faker\Provider;

use Exception;

class Address extends Base
{
    protected static array $citySuffix = ['Ville'];
    protected static array $streetSuffix = ['Street'];
    protected static array $cityFormats = [
        '{{firstName}}{{citySuffix}}',
    ];
    protected static array $streetNameFormats = [
        '{{lastName}} {{streetSuffix}}',
    ];
    protected static array $streetAddressFormats = [
        '{{buildingNumber}} {{streetName}}',
    ];
    protected static array $addressFormats = [
        '{{streetAddress}} {{postcode}} {{city}}',
    ];

    protected static array $buildingNumber = ['%#'];
    protected static array $postcode = ['#####'];
    protected static array $country = [];

    /**
     * @return string
     * @throws Exception
     * @example 'town'
     *
     */
    public static function citySuffix(): string
    {
        return static::randomElement(static::$citySuffix);
    }

    /**
     * @return string
     * @throws Exception
     * @example 'Avenue'
     *
     */
    public static function streetSuffix(): string
    {
        return static::randomElement(static::$streetSuffix);
    }

    /**
     * @return string
     * @throws Exception
     * @example '791'
     *
     */
    public static function buildingNumber(): string
    {
        return static::numerify(static::randomElement(static::$buildingNumber));
    }

    /**
     * @return string
     * @throws Exception
     * @example 'Sashabury'
     *
     */
    public function city(): string
    {
        $format = static::randomElement(static::$cityFormats);

        return $this->generator->parse($format);
    }

    /**
     * @return string
     * @throws Exception
     * @example 'Crist Parks'
     *
     */
    public function streetName(): string
    {
        $format = static::randomElement(static::$streetNameFormats);

        return $this->generator->parse($format);
    }

    /**
     * @return string
     * @throws Exception
     * @example '791 Crist Parks'
     *
     */
    public function streetAddress(): string
    {
        $format = static::randomElement(static::$streetAddressFormats);

        return $this->generator->parse($format);
    }

    /**
     * @return string
     * @throws Exception
     * @example 86039-9874
     *
     */
    public static function postcode(): string
    {
        return static::toUpper(static::bothify(static::randomElement(static::$postcode)));
    }

    /**
     * @return string
     * @throws Exception
     * @example '791 Crist Parks, Sashabury, IL 86039-9874'
     *
     */
    public function address(): string
    {
        $format = static::randomElement(static::$addressFormats);

        return $this->generator->parse($format);
    }

    /**
     * @return string
     * @throws Exception
     * @example 'Japan'
     *
     */
    public static function country(): string
    {
        return static::randomElement(static::$country);
    }

    /**
     * Uses signed degrees format (returns a float number between -90 and 90)
     *
     * @param float|int $min
     * @param float|int $max
     *
     * @return float
     * @throws Exception
     * @example '77.147489'
     *
     */
    public static function latitude($min = -90, $max = 90): float
    {
        return static::randomFloat(6, $min, $max);
    }

    /**
     * Uses signed degrees format (returns a float number between -180 and 180)
     *
     * @param float|int $min
     * @param float|int $max
     *
     * @return float
     * @throws Exception
     * @example '86.211205'
     *
     */
    public static function longitude($min = -180, $max = 180): float
    {
        return static::randomFloat(6, $min, $max);
    }

    /**
     * @return float[]
     * @throws Exception
     * @example array('77.147489', '86.211205')
     *
     */
    public static function localCoordinates(): array
    {
        return [
            'latitude' => static::latitude(),
            'longitude' => static::longitude(),
        ];
    }
}
