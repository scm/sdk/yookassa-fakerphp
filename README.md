# YooKassa FakerPHP

[![Latest Stable Version](https://img.shields.io/packagist/v/yoomoney/yookassa-fakerphp?label=stable)](https://packagist.org/packages/yoomoney/yookassa-fakerphp)
[![Total Downloads](https://img.shields.io/packagist/dt/yoomoney/yookassa-fakerphp)](https://packagist.org/packages/yoomoney/yookassa-fakerphp)
[![Monthly Downloads](https://img.shields.io/packagist/dm/yoomoney/yookassa-fakerphp)](https://packagist.org/packages/yoomoney/yookassa-fakerphp)
[![License](https://img.shields.io/packagist/l/yoomoney/yookassa-fakerphp)](https://packagist.org/packages/yoomoney/yookassa-fakerphp)

Faker - это библиотека PHP, которая генерирует рандомные данные.

## Требования
PHP >= 7.4

1. [Установите менеджер пакетов Composer](https://getcomposer.org/download/).
2. В консоли выполните команду:
```bash
composer require yoomoney/yookassa-fakerphp
```

### В файле composer.json своего проекта
1. Добавьте строку `"yoomoney/yookassa-fakerphp": "^1.0"` в список зависимостей вашего проекта в файле composer.json:
```
...
    "require": {
        "php": ">=7.4",
        "yoomoney/yookassa-fakerphp": "^1.0"
...
```
2. Обновите зависимости проекта. В консоли перейдите в каталог, где лежит composer.json, и выполните команду:
```bash
composer install
```
3. В коде вашего проекта подключите автозагрузку файлов:
```php
require __DIR__ . '/vendor/autoload.php';
```

## Начало работы

Используйте `Faker\Factory::create()` для создания и инициализации генератора Faker.

```php
<?php
require_once 'vendor/autoload.php';

// Используйте factory для создания экземпляра Faker\Generator
$faker = Faker\Factory::create();
// Генерация данных с помощью вызова метода
echo $faker->name();
// 'Иван Иванов'
echo $faker->email();
// 'walter.sophia@hotmail.com'
echo $faker->text();
// 'Съешь ещё этих мягких французских булок да выпей чаю.'
```

